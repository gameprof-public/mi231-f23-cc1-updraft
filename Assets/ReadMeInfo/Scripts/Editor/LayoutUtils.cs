﻿// Original file https://gist.github.com/LotteMakesStuff/9c623cfa51e256baf183c357450cdbc8
// Small modifications by Jeremy G. Bond <jeremy@exninja.com>
// put me in an Editor folder
using System.IO;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public static class LayoutUtils
{
    //// unity stores layouts in a path referenced in WindowLayout.LayoutsPreferencesPath.
    //// Unfortunately, thats internal - well just creat the path in the same way they do!
    //// github.com/Unity-Technologies/UnityCsReference/blob/master/Editor/Mono/GUI/WindowLayout.cs#L980
    //static string layoutsPreferencesPath => Path.Combine(InternalEditorUtility.unityPreferencesFolder, "Layouts");
    
    //[MenuItem("Window/LayoutUtils/Import Editor Layout", priority = 10000)]
    //static void ImportEditorLayout()
    //{
    //    Debug.Log("Attempting to import layout: " + Selection.activeObject.name);
        
    //     // install the layout file by copying it to the layout folder
    //     string sourcePath = AssetDatabase.GetAssetPath(Selection.activeObject);
    //     // https://github.com/Unity-Technologies/UnityCsReference/blob/master/Editor/Mono/GUI/WindowLayout.cs#L1088
    //     string destinationPath = Path.Combine(layoutsPreferencesPath, Selection.activeObject.name + ".wlt");
    //     File.Copy(sourcePath, destinationPath, true);
         
    //     // instruct unity to reload layouts
    //     InternalEditorUtility.ReloadWindowLayoutMenu();
    //}

    //[MenuItem("Window/LayoutUtils/Import Editor Layout", priority = 10000, validate = true)]
    //static bool ValidateImportEditorLayout()
    //{
    //    // we only work on a single selected asset...
    //    if (Selection.objects.Length != 1)
    //        return false;

    //    // lets check if the asset is a layout file
    //    string path = AssetDatabase.GetAssetPath(Selection.activeObject);
    //    return path.EndsWith(".wlt");
    //}
        
    //[MenuItem("Window/LayoutUtils/Open Editor Layouts folder", priority = 10000)]
    //static void OpenlayoutsFolder()
    //{
    //    //Debug.Log("Attempting to import layout: " + Selection.activeObject.name);
    //    Debug.Log("Opening Editor Layouts folder.");
    //    EditorUtility.RevealInFinder(layoutsPreferencesPath+"/default");
    //}
}
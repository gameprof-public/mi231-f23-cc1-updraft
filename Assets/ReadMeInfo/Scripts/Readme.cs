﻿using System;
using UnityEngine;
//using UnityEditor.VersionControl;

public class Readme : ScriptableObject {
	public Texture2D icon;
	public string title;
	public Section[] sections;
	public bool loadedLayout;
	//public Asset layoutAsset;
	public String layoutName;
	
	[Serializable]
	public class Section {
		public string heading, text, linkText, url;
	}
}

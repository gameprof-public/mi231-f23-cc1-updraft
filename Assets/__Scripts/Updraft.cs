﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sets up a basic angularVelocity on the Rigidbody so that it rotates.
/// When a GameObject with the Component PlaneControls exits this Trigger
///   it will pick a new random location within the repositionRange.
/// </summary>
[RequireComponent( typeof(Rigidbody) )]
[DisallowMultipleComponent]
public class Updraft : MonoBehaviour {
    public float   rotSpeed           = 10f;
    public Vector3 repositionRangeMax = new Vector3( 256, 0, 256 );
    public Vector3 repositionRangeMin = new Vector3( -256, 0, -256 );

    void Start() {
        // Set the angularVelocity of the Rigidbody component to
        //  Vector3(0, rotSpeed, 0);
    }

    private void OnTriggerExit( Collider other ) {
        // If other has a PlaneControls component, then the plane has just
        //  exited this Updraft.
        {
            // Call the Reposition() method on this Updraft
        }
    }

    /// <summary>
    /// Chooses a position for this Updraft between repositionRangeMin and Max
    /// </summary>
    void Reposition() {
        // Set x, y, and z values of a Vector3 pos from repositionRange values

        // Set transform.position to pos
    }
}

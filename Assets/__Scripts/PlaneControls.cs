﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rotates this GameObject around y based on rotVel and Input.GetAxis("Horizontal")
/// The model is rotated around z to make the model of the plane roll left and right
/// The plane yVel is increased to yVelMax when inside an Updraft Trigger, and when not
///   in an Updraft, it will decrease by yVelGravity * Time.fixedDeltaTime every
///   FixedUpdate, but it never falls below yVelMin.
/// Look for Input.GetKeyDown(Keycode.Space) in Update() to boost, but you should
///   only boost when the BOOST_RECHARGE_U == 1.
/// Boost increases speed to boostSpeed immediately, and then speed interpolates
///   down based on the equation:
///     float boostU = (Time.time - boostTime) / boostDuration;
///     boostU = Mathf.Clamp01(boostU);
///     speed = (1 - boostU) * boostSpeed + boostU * baseSpeed;
/// </summary>
[DisallowMultipleComponent]
public class PlaneControls : MonoBehaviour {
    // BoostSlider can get this value as PlaneControls.BOOST_RECHARGE_U, but
    //  BOOST_RECHARGE_U can only be set by the PlaneControls class
    static public float BOOST_RECHARGE_U { get; private set; }

    [Header("Inscribed")]
    public Transform model;
    public float baseSpeed      = 40;   // The normal speed for the plane
    public float boostSpeed     = 200;  // The maximum boost speed
    public float boostDuration  = 1;    // # of seconds that boost lasts
    public float boostRecharge  = 10;   // # of seconds boost takes to recharge
    public float rotVel         = 40;   // Degrees the plane turns per second
    public float rollMax        = -60;  // The amount that the plane rolls
    public float yVelMax        = 20;   // Maximum value for yVel in Updrafts
    public float yVelMin        = -5;   // Minimum value for yVel
    public float yVelGravity    = -8;   // Gravity applied per second to yVel

    [Header("Dynamic")]
    public float speed          = 40;   // The current speed
    public float yVel           = 0;    // The current y velocity
    public float lastBoostTime  = -10;  // The time of the last boost
    // Note: lastBoostTime begins already set to -10, which allows the player
    //  to immediately boost when the game starts.

    Rigidbody rigid;

    void Start()
    {
        // speed should be set to the baseSpeed

        // Set rigid to be the Rigidbody component of this GameObject
        
        // Set rigid.velocity to transform.forward * speed
        
    }

    private void Update()
    {
        // if BOOST_RECHARGE_U == 1 and the player presses space...
        {
            // Set speed to boostSpeed and lastBoostTime to the current time.

        }
    }

    void FixedUpdate()
    {
        // Get value of the Horizontal Input axis

        // Create a float boostU local variable to hold the amount of boost that should
        //  be applied right now.
        
        // Set boostU to (time since lastBoostTime) / boostDuration so that the boost
        //  lasts boostDuration seconds. 

        // Clamp that boostU value so that it never goes above 1

        // Set speed based on (1 - boostU) * boostSpeed + boostU * baseSpeed

        // BOOST_RECHARGE_U varies from 0 to 1 based on time since lastBoostTime
        //  divided by boostRecharge. Clamp it so that it doesn't go above 1.

        // Multiply the Horizontal axis value by rotVel and fixedDeltaTime to get
        //  the amount of rotation around y to apply via transform.Rotate this FixedUpdate()
        
        // Apply that rotation around y (the "pan" axis that rotates left and right)
        //  by calling transform.Rotate() 

        // Set model.localRotation to roll (i.e., bank) the model of the plane left & right
        //  as the plane turns. The "roll" axis is the local z axis of the Transform named model.
        //  The roll value should be Horizontal axis * rollMax

        // Add yVelGravity * fixedDeltaTime to yVel (yVelGravity is negative)

        // Clamp yVel so that it stays between yVelMin and yVelMax

        // Set rigid.velocity to (transform.forward times speed) plus (yVel times Vector3.up)

    }

    void OnTriggerStay(Collider other)
    {
        // Since other is a collider on the child of the Updraft GameObject
        // If other finds an Updraft component on its parent...
        {
            // Set yVel to yVelMax

        }
    }
}

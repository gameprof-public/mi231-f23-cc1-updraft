﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This follows the y position of the keepBelowTransform, making sure that this
/// GameObject stays at least howFarBelow units below the y of keepBelowTransform.
/// </summary>
[DisallowMultipleComponent]
public class KeepBelow : MonoBehaviour
{
    public Transform keepBelowTransform;
    public float howFarBelow = 2;

    void FixedUpdate()
    {
        // If transform.position.y >= keepBelowTransform.position.y - howFarBelow
        {
            // Then move transform.position to a y position howFarBelow under keepBelowTransform.position.y 

        }
    }
}

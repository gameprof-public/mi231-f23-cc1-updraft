﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manages the BoostSlider. This should connect to the Slider component and set
/// its value based on PlaneControls.BOOST_RECHARGE_U.
/// You should also set the color of the slider based on its value.
/// </summary>
[RequireComponent(typeof(Slider))]
[DisallowMultipleComponent]
public class BoostSlider : MonoBehaviour
{
    [Header("Inscribed")]
    public Color rechargeColor  = Color.red;
    public Color readyColor     = Color.green;
    public Image fillImage;

    Slider slid;

    void Start()
    {
        // Set slid to the Slider component attached to this GameObject

    }

    void FixedUpdate()
    {
        // Set slid.value to PlaneControls.BOOST_RECHARGE_U


        // Set fillImage.color to readyColor if BOOST_RECHARGE_U == 1
        // Otherwise, set fillImage.color to rechargeColor
        // Try using a "ternary operator": e.g., bool b = (u==1) ? true : false;

    }
}

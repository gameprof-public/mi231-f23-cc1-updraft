﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is used to make FollowingFog move its x and z position along with the
///  plane while keeping the Y position of this object =0 so that it stays
///  on the ground.
/// In this game, the plane is pointOfInterest.
/// </summary>
[DisallowMultipleComponent]
public class GroundFollow : MonoBehaviour
{
    public Transform pointOfInterest;

    void FixedUpdate()
    {
        // Set the position of transform to the position of pointOfInterest, but
        //  make sure that the y is locked to 0

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Reloads the entire scene whenever the plane collides with anything
/// </summary>
[DisallowMultipleComponent]
public class ReloadOnCollision : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        // Reload Scene 0
        
    }
}
